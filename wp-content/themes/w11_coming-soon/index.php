<?php get_header(); ?>
 <div class="topleft">
    <div class="logo">
      <?php
  if(function_exists('the_custom_logo')) {
    the_custom_logo();
  }
  ?>
    </div>
    
  </div>
  <?php if ( get_header_image() ) : ?>
    <img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>"
  <?php endif; ?>
  <div class="middle">
    <h1>COMING SOON</h1>
    <hr>
	<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; ?>
	<?php endif; ?>
    <hr>
    <p id="demo" style="font-size:30px">&nbsp;</p>
  </div>
  <div class="bottomleft">
    <p>Some text</p>
  </div>
<?php get_footer(); ?>
