<?php

register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'comming-soon' ),
	) );



function my_theme_enqueue_scripts() {
	
	// Adding family font
	wp_enqueue_style('ubuntu-font', 'https://fonts.googleapis.com/css?family=Ubuntu:400,400i,700,700i' );

	wp_enqueue_style('coming-soon-style', get_stylesheet_uri());

	wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '20151215', true );

	wp_enqueue_script('coming-soon-js', get_template_directory_uri() . '/assets/js/main.js', array(), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'my_theme_enqueue_scripts');

function coming_soon_activated() {
	update_option('show_on_front', 'page');
}

add_action('after_switch_theme', 'coming_soon_activated');


function coming_soon_setup() {
	add_theme_support('title-tag');
	// Add theme to support logo customization
	add_theme_support('custom-logo');
	add_theme_support('custom-header');
}
add_action('after_setup_theme', 'coming_soon_setup');
