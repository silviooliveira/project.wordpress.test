<?php /*Template Name: Two Column, Form Left */

get_template_part('template-parts/header', 'bare' ); 

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main container" role="main">
			<div class="row">
				<div class="col-md-6">
					<iframe src="https://docs.google.com/forms/u/0/d/e/1FAIpQLScLQaJFnIU9-nP-br0UmaL8-SMRhrPAbczDdoR-FX3vH93LLQ/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
				</div>
				<div class="col-md-6">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_template_part('template-parts/footer', 'bare' ); 