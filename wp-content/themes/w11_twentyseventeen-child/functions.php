<?php
function my_theme_enqueue_styles() {
	// Adding family font
	wp_enqueue_style('ubuntu-font', 'https://fonts.googleapis.com/css?family=Ubuntu:400,400i,700,700i' );
	
	wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

?>