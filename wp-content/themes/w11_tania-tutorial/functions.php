<?php

// Bootstrap Navgation
require_once('wp-bootstrap-navwalker.php');

register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'w11_tania-tutorial' ),
) );