<?php
/**
 * w11_starter functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package w11_starter
 */

if ( ! function_exists( 'w11_starter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function w11_starter_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on w11_starter, use a find and replace
	 * to change 'w11_starter' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'w11_starter', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	add_theme_support( 'site-tagline' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	// register_nav_menus( array(
	// 	'menu-1' => esc_html__( 'Primary', 'w11_starter' ),
	// ) );

	register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'w11_starter_bootstrap' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'w11_starter_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Add theme to support logo customization
	add_theme_support('custom-logo');
}
endif;
add_action( 'after_setup_theme', 'w11_starter_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function w11_starter_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'w11_starter_content_width', 640 );
}
add_action( 'after_setup_theme', 'w11_starter_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function w11_starter_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'w11_starter' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'w11_starter' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'w11_starter_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function w11_starter_scripts() {

	// Adding family font
	wp_enqueue_style('ubuntu-font', 'https://fonts.googleapis.com/css?family=Ubuntu:400,400i,700,700i' );

	wp_enqueue_style('w11_starter-style', get_stylesheet_uri());

	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css');
	
	wp_enqueue_style('bootstrap-theme', get_template_directory_uri() . '/assets/css/bootstrap-theme.css');

	wp_enqueue_style('bootstrap-lavish', get_template_directory_uri() . '/assets/css/lavish-bootstrap.css');

	wp_enqueue_style('coming-soon', get_template_directory_uri() . '/assets/css/coming-soon.css');

	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'w11_starter-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'w11_starter-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	//wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/main.js');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'w11_starter_scripts' );

/** Create new post type **/

function mytheme_video_post_type() {
	register_post_type('video',
						[
							'labels' => [
								'name' => __('Videos'),
								'singular_name)' => __('Video'),
							],
							'public' => true,
							'has_archive' => true,
							'menu_icon' => 'dashicons-video-alt3'
						]
		);
}
add_action('init', 'mytheme_video_post_type');

/**
Add Video Type to Search
**/

function mytheme_add_custom_post_types($query) {

	if (is_home() && $query->is_main_query()) {
		$query->set('post_type', ['post', 'video']);
	}
	return $query;
}

add_action('pre_get_posts', 'mytheme_add_custom_post_types');

function css_background_image_override($selector, $image) {
	$style = '';
	$template = "<style>{selector}{background-image:url('{image}') }</style>";
	if ( $image ) {
		$style = $template;
		$style = str_replace('{selector}', $selector, $style);
		$style = str_replace('{image}', $image, $style);
	}
	echo $style;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Additional features to allow styling of the templates.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/* Navbar config for bootsrap */
require_once('wp-bootstrap-navwalker.php');
