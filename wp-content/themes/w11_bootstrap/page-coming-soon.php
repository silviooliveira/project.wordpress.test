<?php get_template_part('template-parts/header', 'coming-soon' ); ?>
 <div class="topleft">
    <p class="logo">Logo</p>
  </div>
  <div class="middle">
    <h1>COMING SOON</h1>
    <hr>
	<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; ?>
	<?php endif; ?>
    <hr>
    <p id="demo" style="font-size:30px">&nbsp;</p>
  </div>
  <div class="bottomleft">
    <p>Some text</p>
  </div>
<?php get_template_part('template-parts/footer', 'coming-soon' ); ?>
